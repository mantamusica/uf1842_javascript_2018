//Creamos número función de número aleatorio
		function numero_aleatorio(){
			return Math.floor ((Math.random() * 8)+1);
		}
//Esta función la usamos para que cada vez que iniciemos el juego
//el número de filas y columnas sea diferente para ello, creamos 
//variable de filas y columnas y a cada una le entregamos un valor
//aleatorio creado por la función.

var fila_premiada = numero_aleatorio();
var columna_premiada = numero_aleatorio();
console.log(fila_premiada,columna_premiada)
//Creamos la variable donde alojamos el tope de intentos
var record = 64;

//Después de haber puesto en el html que cada celda da dos valores(x,y)
//crear función para esos dos valores.
function comprobar_celda(fila,columna){
	//Haciendo click en la celda recoge un valor para hacer la comprobación en el if
	var celda_pulsada = document.getElementById(fila+"-"+columna);
	//Cada intento fallido resta uno en el texto de la portada
	record = record-1;
	document.getElementById("texto_record").innerHTML = "Record de intentos: "+record;
	//console.log("Has pulsado la celda ("+fila+","+columna+") y no es esta, sigue intentandolo");
	if(fila==fila_premiada && columna==columna_premiada){
		for(var i=1;i<=8;i++){
			for(var j=1;j<=8;j++){
				var mi_celda = document.getElementById(i+"-"+j);
				mi_celda.className = "celda_error";//Con esta orden llama a la clase creada en CSS
				mi_celda.onclick = "";//Con esta orden la celda queda inhabilitada, en blanco
			}
		}
		celda_pulsada.className = "celda_premiada";//Con esta orden llama a la clase creada en CSS
		celda_pulsada.onclick = "";//Con esta orden la celda queda inhabilitada, en blanco
		//console.log("Has pulsado la celda ("+fila+","+columna+") y acertaste");
	}else{
		celda_pulsada.className = "celda_error";//Con esta orden llama a la clase creada en CSS
		celda_pulsada.onclick = "";//Con esta orden la celda queda inhabilitada, en blanco
		//console.log("Has pulsado la celda ("+fila+","+columna+") y Fallaste");
	}
}
function recargar(){
	window.location.href ="index.html";
}
