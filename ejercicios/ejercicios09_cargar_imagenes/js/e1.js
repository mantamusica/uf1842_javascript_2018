// JavaScript Document
window.onload = cargar;

var fotos = new Array("AlosToros", "AyuntamientoMadrid", "BancodeEspaña", "BasilicadeAtochayTorrenueva", "BodaReales-ConcursoAerostatico",
 "BodasReales-CaravanaAutomovilistaanteS", "BodasReales-PzaCastelarcomitivaRegia", "BolsadeComercio");
var imagenes = new Array();

function cargar() {
	for (var i = 0; i < fotos.length; i++) {
		imagenes[i] = new Image();
		imagenes[i].src="images/"+fotos[i]+".jpg";
	}

	for (var i = 0; i < fotos.length; i++) {
		document.querySelectorAll(".cards")[i].src = "images/thumbs/"+fotos[i]+".jpg";
}

	for (var i = 0; i < fotos.length; i++) {
		document.querySelectorAll(".cards")[i].onclick = ejecutar;
	}

	document.querySelector(".ocultar").style.display = "none";
}
	function ejecutar(event){
			var indice = event.target.getAttribute("data-indice");
			var caja = document.querySelector(".ocultar");
			caja.style.backgroundImage = "url('"+imagenes[indice].src+"')";
			caja.style.display = "block";
	}

	function cerrar() {
	    document.querySelector(".ocultar").style.display = "none";
	    document.querySelector(".ocultar").style.backgroundImage = "none";

	}
