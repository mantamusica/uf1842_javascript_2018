window.addEventListener("load",inicio);

var canvas;
var ctx;
var botones;


function inicio(){
	canvas = document.querySelector("canvas");
	ctx = canvas.getContext("2d");

	botones = document.querySelectorAll("div>ul>li");

	canvas.addEventListener("mousemove", mostrar);
	canvas.addEventListener("mousemove", dentro);

	for (var i = 0; i < botones.length; i++) {
		botones[i].addEventListener("click", function(event){
			ctx.strokeStyle = event.target.getAttribute("data-color");
		});

	}
}

function dentro(event){
	ctx.beginPath();
	ctx.lineWidth = 0.5;//cambia la anchura de la linea
	ctx.moveTo(event.clientX-event.target.offsetLeft, event.clientY-event.target.offsetTop);
}

function mostrar(event){
	ctx.lineTo(event.clientX-event.target.offsetLeft, event.clientY-event.target.offsetTop);
	ctx.stroke();
}



