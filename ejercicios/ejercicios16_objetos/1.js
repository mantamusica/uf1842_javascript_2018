/* Estamos creando una clase*/
var Punto=function(){
	// los siguientes miembros son propiedades privadas
	var x;
	var y;
	var sup=this; // esto es para poder utilizar dentro de metodos privados las propiedades publicas
	// las siguientes son públicas
	this.color;
	this.ancho;
	
	// los siguientes  metodos son publicas
	this.getX=function(){
		return x;
	};
	
	this.getY=function(){
		return y;
	};
	
	this.setX=function(valor){
		x=valor; 
	};
	
	this.setY=function(valor){
		y=valor;
	};
	
	this.dibujar=function(){
		document.querySelector("body").innerHTML+='<div class="punto" style="left:'+x+'px;top:'+y+'px;border-radius:'+this.ancho+'px;width:'+this.ancho+'px;height:'+this.ancho+'px;background-color:'+this.color+'"></div>'	;
	};
	
	
	// las siguientes son metodos privados denominado constructor

	function constructor(){
		x=0;
		y=0;
		this.color="black";
		this.ancho="10"
	}
	constructor();
};



// esto es para gestionar el evento de cargar la pagina
window.addEventListener("load",function (){
	/* Estamos creando una clase */
	var punto1=new Punto();
	// quiero dibujar un punto en la posicion 50,100 de color negro con tamaño 10
	punto1.setX(50); 
	punto1.setY(100);
	punto1.color = "black";
	punto1.ancho = "10";
	punto1.dibujar();
	// quiero dibujar un punto en la posicion 250,200 de color rojo con tamaño 20;
	//punto1.setX(250); 
	//punto1.setY(200);
	//punto1.color('red');
	//punto1.ancho(20);
	// faltan lineas
	//punto1.dibujar();
});
