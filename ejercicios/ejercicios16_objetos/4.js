/* Estamos creando una clase */
var Punto=function(){
	// los siguientes miembros son propiedades 
	// las siguientes son privadas
	var x;
	var y;
	var sup=this; // esto es para poder utilizar dentro de metodos privados las propiedades publicas
	// las siguientes son publicas
	this.color;
	this.ancho;
	
	// los siguientes miembros son metodos
	// las siguientes son publicas
	this.getX=function(){
		return x;
	};
	
	this.getY=function(){
		return y;
	};
	
	this.setX=function(valor){
		x=valor; 
	};
	
	this.setY=function(valor){
		y=valor;
	};
	
	this.dibujar=function(){
		document.querySelector("body").innerHTML+='<div class="punto" style="left:'+x+'px;top:'+y+'px;border-radius:'+this.ancho+'px;width:'+this.ancho+'px;height:'+this.ancho+'px;background-color:'+this.color+'"></div>'	;
	};
	
	
	// las siguientes son privadas
	// el siguiente metodo es el que se denomina constructor
	function constructor(){
		x=0;
		y=0;
		sup.color="BLACK";
		sup.ancho="10";
	};
	constructor();
};

/* Estamos creando un objeto */
var punto1=new Punto(); 

// esto es para gestionar el evento de cargar la pagina
window.addEventListener("load",cargar);

function cargar(){
	// quiero dibujar un punto en la posicion 50,100 de color negro con tamaño 10
	punto1.setX(50); 
	punto1.setY(100);
	punto1.dibujar();
	
	// quiero dibujar un punto en la posicion 250,200 de color rojo con tamaño 20;
	punto1.setX(250);
	punto1.setY(200);
	punto1.ancho=20;
	punto1.color="RED";
	punto1.dibujar();
	
	var punto2=new Punto();
	punto2.setX(23);
	punto2.setY(33);
	punto2.ancho=6;
	punto2.color="RED";
	
	punto2.dibujar();
	punto2.setX(33);
	punto2.setY(100);
	punto2.ancho=10;
	punto2.color="BLUE";
	punto2.dibujar();
	
	punto2.setX(500);
	punto2.setY(50);
	punto2.ancho=22;
	punto2.color="GREEN";
	punto2.dibujar();
	
	punto2.setX(23);
	punto2.setY(500);
	punto2.ancho=33;
	punto2.color="YELLOW";
	punto2.dibujar();
	
	
}
