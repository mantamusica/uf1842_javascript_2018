/* Estamos creando una clase */
var Punto=function(){
	
	var x;
	var y;
	var color;
	var ancho;
	var objeto;
	var sup=this;

	this.getX=function(){
		return x;
	};
	
	this.getY=function(){
		return y;
	};
	
	this.setX=function(valor){
		borrar();
		x=valor; 
		this.dibujar();
	};
	
	this.setY=function(valor){
		borrar();
		y=valor;
		this.dibujar();
	};
	
	this.dibujar=function(){
		/* debeis corregir el siguiente codigo */
		document.querySelector("body").innerHTML+='<div class="punto" style="left:'+x+'px;top:'+y+'px;border-radius:'+ancho+'px;width:'+ancho+'px;height:'+ancho+'px;background-color:'+color+'"></div>'	;
		objeto=document.querySelector("body").lastElementChild;
	};
	
	function borrar(){
		document.querySelector("body").removeChild(objeto);
	}
	
	this.getColor=function(){
		return color;	
	};
	
	this.setColor=function(dato){
		borrar();
		color=dato;	
		this.dibujar();
	};
	
	this.getAncho=function(){
		return ancho;
	}
	
	this.setAncho=function(dato){
		borrar();
		ancho=dato;
		this.dibujar();
	}
	
	// las siguientes son privadas
	// el siguiente metodo es el que se denomina constructor
	function constructor(){
		x=0;
		y=0;
		color="WHITE";
		ancho="10";
		sup.dibujar();
	};
	constructor();
};




// esto es para gestionar el evento de cargar la pagina
window.addEventListener("load",cargar);

function cargar(){
	var punto1=new Punto(); 
	punto1.setX(50); 
	punto1.setY(100);
	
	var punto2=new Punto();
	punto2.setX(23);
	punto2.setY(33);
	punto2.setAncho(6);
	punto2.setColor("RED");
	
	var punto3=new Punto();
	punto3.setX(33);
	punto3.setY(100);
	punto3.setAncho(10);
	punto3.setColor("BLUE");
	
	var punto4=new Punto();
	punto4.setX(500);
	punto4.setY(50);
	punto4.setAncho(22);
	punto4.setColor("GREEN");
	
	var punto5=new Punto();
	punto5.setX(23);
	punto5.setY(500);
	punto5.setAncho(33);
	punto5.setColor("YELLOW");

}
