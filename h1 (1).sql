-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-10-2017 a las 19:10:06
-- Versión del servidor: 10.1.16-MariaDB
-- Versión de PHP: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `h1`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entradas`
--

CREATE TABLE `entradas` (
  `id` int(11) NOT NULL,
  `texto` varchar(5000) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `titulo` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `foto` varchar(20) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1820 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `entradas`
--

INSERT INTO `entradas` (`id`, `texto`, `titulo`, `foto`) VALUES
(1, 'Para realizar una nueva conexión se debe crear una instancia del objeto PDO. Este constructor acepta una serie de parámetros de conexión (string de conexión) que pueden ser específicos para cada sistema de bases de datos.\r\n\r\nSi no se logra establecer la conexión se producirá una excepción (PDOException). Si la conexión es exitosa, una instancia de PDO será devuelta. La conexión permanece activa por todo le ciclo de vida del objeto PDO. Para cerrar la conexión, se debe destruir el objeto asegurándose que toda referencia sea eliminada, o bien, PHP cerrará la conexión automáticamente cuando el programa finalice.\r\n\r\nSi se desea hacer una conexión persistente, que no sea eliminada al final de la ejecución del programa, es necesario habilitar la opción PDO:ATTR_PERSISTENT en el arreglo de las opciones de la conexión.', 'PDO', 'f1.jpg'),
(2, 'Ésta es, generalmente, la primera pregunta que se formula sobre los estándares web. Se puede crear el contenido, el estilo y el formato sólo utilizando HTML: elementos de tipo de letra para el estilo y tablas HTML para el formato, de manera que, ¿por qué preocuparse de este asunto de XHTML/CSS? El uso de tablas para el formato, etc., es como se solía hacer en los malos tiempos del diseño web, y mucha gente todavía lo hace de esta manera (aunque no se debería de hacer) y, de hecho, es uno de los motivos por los que hemos creado este curso. Aquí no trataremos estos métodos. Éstas son las razones más importantes para utilizar CSS y HTML en vez de métodos obsoletos:\r\n\r\nEficiencia del código: cuanto mayores sean los archivos, más tardarán en descargarse y más dinero le costará a algunas personas (algunas personas todavía pagan por megabyte descargado y las tarifas móviles acostumbran a tener límites estrictos). Por lo tanto, no se debe malgastar el ancho de banda con páginas grandes abarrotadas de información de estilo y de formato en cada archivo HTML. Una alternativa mucho mejor es que los archivos HTML estén desglosados y limpios, e incluir la información de estilo y de formato sólo una vez en un archivo CSS independiente o en varios.\r\n\r\nEficiencia del código\r\nPara ver un caso real de este hecho en acción, podéis ver el artículo sobre la reescritura de Slashdot, en A List Apart Slashdot, donde el autor tomó una página web muy popular y la reescribió en XHTML/CSS.\r\nFacilidad de mantenimiento: en relación con el último punto, si la información de estilo y formato sólo se especifica en un sitio, quiere decir que sólo habrá que hacer actualizaciones en un lugar si se quiere cambiar el aspecto de la página web. ¿Preferiríais tener que actualizar esta información en cada página de vuestra web? No lo creo.\r\n\r\nAccesibilidad: los usuarios de la web con problemas visuales pueden utilizar una tipo de software conocido como "lector de pantalla" para acceder a la información mediante el sonido en vez de la vista; literalmente, les lee la página. Además, el software de introducción de datos mediante la voz que utilizan las personas con problemas de movilidad también se beneficia de las páginas web con una semántica bien construida. De manera muy parecida al lector de pantalla que utiliza las instrucciones del teclado para navegar por los encabezamientos, enlaces y formularios, un usuario que interaccione interaccione mediante voz utilizará instrucciones con su voz. Los documentos web marcados semánticamente, en vez de presentacionalmente, pueden resultar más fáciles de navegar y la información que contienen es más fácil de encontrar por parte del usuario. En otras palabras, cuanto más rápidamente "entre en materia" (el contenido), mejor. Los lectores de pantalla no pueden acceder al texto dentro de imágenes y encuentran confusos algunos usos de JavaScript. Aseguraos de que el contenido más importante está disponible para todo el mundo.', '¿Porque separar?', 'f2.jpg'),
(3, 'Probadlo; introducid este código en vuestro documento HTML de muestra y cargadlo en un navegador, o id a la página del primer ejemplo de formulario básico para navegar hasta el formulario en otra página. Intentad jugar un poco con los diferentes controles', 'Documento HTML', 'f3.jpg'),
(4, 'Si la seguridad de los datos del formulario es un aspecto que os preocupa especialmente, por ejemplo si enviáis un número de tarjeta de crédito a un sitio de compras, entonces deberíais utilizar el protocolo https con un protocolo de capa de conexión segu', 'Seguridad formulario', 'f4.jpg'),
(5, 'Eficiencia del código: como aprenderéis a lo largo del curso, gran parte de las mejores prácticas en el uso de estándares web se basan en la reutilización del código: en el hecho de que se pueda separar el contenido HTML de la información de estilo (CSS) ', 'Reutilizacion del codigo', 'f5.jpg'),
(6, 'En el formulario anterior del paso tres hemos añadido una casilla de selección (checkbox) con el fin de mostrar que se pueden utilizar los atributos adicionales del elemento input para recoger información que queda más allá de la introducción de texto en ', 'Formulario', 'f6.jpg'),
(7, 'Compatibilidad de dispositivos: con esto nos referimos al hecho de garantizar que sus sitios web funcionarán no sólo en diferentes plataformas es decir, Windows, Mac, Linux, sino también con dispositivos de navegación alternativos, como los que hoy día ', 'Compatibilidad', 'f7.jpg'),
(8, 'Políticas de empresa: no cabe duda de que algunas empresas/instituciones todavía tienen sitios web realmente anticuados y obsoletos. Puede que tengan políticas que obliguen a sus empleados a utilizar navegadores obsoletos, pero la situación está mejorando', 'políticas de empresa', 'f8.jpg'),
(9, 'Hace tiempo, la gente solía hacer cosas como planificar sus sitios web con tablas gigantes, utilizando las diferentes celdas de la tabla para situar sus gráficos, el texto, etc. (la finalidad de las tablas no es ésta y añade un etiquetado superfluo', 'maquetacion', 'f9.jpg'),
(10, 'sdekjrgh ljsefsd jfksdkjkjfhskfh sjhjsfhsfhsfh fhs', 'solarium', ''),
(11, '<p>ejemplo de clase</p>', 'prueba', ''),
(12, '<p>esto es un <strong>ejemplo</strong></p><br />\r\n<div><strong>una caja</strong></div>', 'prueba1', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `entradas`
--
ALTER TABLE `entradas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `entradas`
--
ALTER TABLE `entradas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
