<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>Ejercicio01 - Tablas</title>
        <?php include_once "listar.php"; ?>
        <link rel="stylesheet" type="text/css" href="css/reset.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script type="text/javascript">
            var imagenes = new Array();


            function mostrar(event) {
                var x = event.target.getAttribute("data_id");
                var nombre_parada, titulo, image, texto;
                var llave = 0;
                for (var c = 0; c < datos.length && llave == 0; c++) {
                    if (x == datos[c].id_parada) {
                        document.querySelector('.modal').style.display = 'block';

                        nombre_parada = document.querySelector('.nombre_parada');
                        nombre_parada.innerHTML = datos[c].nombre_parada;

                        image = document.querySelector('.imagen');
                        image.style.backgroundImage = "url(" + datos[c].url_foto + ")";

                        texto = document.querySelector('.texto');
                        texto.innerHTML = datos[c].texto;

                        llave = 1;
                    }
                }
            }

            window.addEventListener('load', function() {

                document.querySelector('.modal').style.display = 'none';
                document.querySelector('.foto').addEventListener('click', function() {
                    document.querySelector('.modal').style.display = 'none';
                });

                var cajas = document.getElementsByTagName("area");

                for (var c = 0; c < cajas.length; c++) {
                    cajas[c].addEventListener("click", mostrar);
                }
            });
        </script>
    </head>

    <body>
        <h1>General Dávila - Valdecilla</h1>
        <div class="tabla"><img src="img/paradas.jpg"  alt="" width="500" height="680" usemap="#Map"/>
            <map name="Map">
                <area shape="rect" coords="161,232,243,243" data_id="0" href="#">
                <area shape="rect" coords="184,255,243,263" data_id="1" href="#">
                <area shape="rect" coords="167,274,241,283" data_id="2" href="#">
                <area shape="rect" coords="205,294,242,303" data_id="3" href="#">
                <area shape="rect" coords="150,315,242,326" data_id="4" href="#">
                <area shape="rect" coords="151,336,243,346" data_id="5" href="#">
                <area shape="rect" coords="151,356,243,365" data_id="6" href="#">
                <area shape="rect" coords="182,375,244,387" data_id="7" href="#">
                <area shape="rect" coords="180,397,242,405" data_id="8" href="#">
                <area shape="rect" coords="191,415,243,425" data_id="9" href="#">
                <area shape="rect" coords="140,437,243,445" data_id="10" href="#">
                <area shape="rect" coords="173,457,244,467" data_id="11" href="#">
                <area shape="rect" coords="135,477,244,487" data_id="12" href="#">
                <area shape="rect" coords="166,499,244,507" data_id="13" href="#">
                <area shape="rect" coords="165,517,244,526" data_id="14" href="#">
                <area shape="rect" coords="306,515,408,522" data_id="15" href="#">
                <area shape="rect" coords="306,490,408,501" data_id="16" href="#">
                <area shape="rect" coords="306,467,380,477" data_id="17" href="#">
                <area shape="rect" coords="305,443,386,453" data_id="18" href="#">
                <area shape="rect" coords="305,420,385,428" data_id="19" href="#">
                <area shape="rect" coords="305,397,386,407" data_id="20" href="#">
                <area shape="rect" coords="306,374,374,382" data_id="21" href="#">
                <area shape="rect" coords="306,350,382,359" data_id="22" href="#">
                <area shape="rect" coords="304,325,386,336" data_id="23" href="#">
                <area shape="rect" coords="306,304,371,312" data_id="24" href="#">
                <area shape="rect" coords="305,279,403,289" data_id="25" href="#">
                <area shape="rect" coords="304,255,410,265" data_id="26" href="#">
                <area shape="rect" coords="306,235,357,241" data_id="27" href="#">
            </map>
        </div>
        <div class="modal">
            <div class="nombre_parada"></div>
            <div class="imagen"></div>
            <div class="texto"></div>
            <div class="foto"></div>

        </div>

    </body>

</html>