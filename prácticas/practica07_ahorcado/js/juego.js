	//Para los intentos
	var juegoPalabras = new Array("javascript", "programar", "jquery", "cliente",
			"servidor", "desarrollo", "codigo", "google","yahoo","independencia","murcielago","arbol");
	var letrasUtilizadas = "";
	var MAXINTENTOS = 9;
	var intentos = 0;
	//Palabra a encontrar
	var palabraOculta = "";
	var palabraActual;
	//Para pedir cada letra
	var letra = "";

	/***
	 * Configuracion del juego
	 ***/

	 window.addEventListener('load', function () {
		document.querySelector('#jugar').addEventListener('click', function(){
			pantalla_reinicio();
			cuentaAtras();

			document.getElementById('validar').style.display = 'block';
			document.getElementById('letra').style.display = 'block';
			document.getElementById('letra').focus();
			document.getElementById('numero').style.display = 'block';
            var index = Math.floor(Math.random() * juegoPalabras.length);
			palabraOculta = juegoPalabras[index].toUpperCase();
			palabraActual = new Array(palabraOculta.length);
			letrasUtilizadas = "";
			intentos = 0;

			for ( var i = 0; i < palabraActual.length; i++) {
			palabraActual[i] = 0;
			}
			
			mostrarInformacion();
			document.getElementById('jugar').style.display = 'none';
        });

		document.querySelector('#reset').addEventListener('click', function(){
            location.reload(true);
        });

        pantalla_reinicio();
	});

	function validarPalabra(letra) {

		var encontroPalabra = false;
		var coincidencias = 0;
		letrasUtilizadas += letra.toUpperCase() + " ";

		//Aqui esta la magia que valida si la palabra existe			
		for ( var i = 0; i < palabraActual.length; i++) {
			if (letra.charAt(0).toUpperCase() == palabraOculta.charAt(i)) {
				palabraActual[i] = 1;
				encontroPalabra = true;
			}
		}

		if (!encontroPalabra) {
			intentos++;
		}
		switch (intentos) {
			case 1:
				document.getElementById('suelo').style.display = 'block';
				break;
			case 2:
				document.getElementById('palo-vertical').style.display = 'block';
				break;
			case 3:
				document.getElementById('palo-superior').style.display = 'block';
				break;
			case 4:
				document.getElementById('cabeza').style.display = 'block';
				break;
			case 5:
				document.getElementById('cuerpo').style.display = 'block';
				break;
			case 6:
				document.getElementById('brazos').style.display = 'block';
				break;
			case 7:
				document.getElementById('piernas').style.display = 'block';
				break;
			case 8:
				document.getElementById('mensaje').style.display = 'block';
				document.getElementById('mensaje').style.backgroundColor = 'white';
				document.getElementById('mensaje').style.border = 'thin solid red';
				document.getElementById('mensaje').style.color = 'red';
				document.getElementById('mensaje').innerHTML = 'Cuidado !!! Estas a puntito de perder.';
				break
			case 9:
				document.getElementById('mensaje').style.display = 'none';
				document.getElementById('cuerda').style.display = 'block';
				document.getElementById('mensaje').style.display = 'none';
				document.getElementById('mensaje').style.display = 'block';
				document.getElementById('mensaje').style.backgroundColor = 'red';
				document.getElementById('mensaje').style.border = 'thin solid red';
				document.getElementById('mensaje').style.color = 'white';
				document.getElementById('mensaje').innerHTML = 'Ohhhh!!! Perdiste.';
				break;
			default:
				alert("el juego se acabó.");
				location.reload(true);
				break;
		}

		mostrarInformacion();

		for ( var i = 0; i < palabraActual.length; i++) {
			if (palabraActual[i] == 1) {
				coincidencias++;
			}
		}
		if (coincidencias == palabraOculta.length) {
			clearInterval(intervalo);
			document.getElementById('mensaje').style.display = 'block';
			document.getElementById('mensaje').style.backgroundColor = 'white';
			document.getElementById('mensaje').style.border = 'thin solid green';
			document.getElementById('mensaje').style.color = 'green';
			document.getElementById('mensaje').innerHTML = 'Felicidades, lo conseguiste.';
			document.getElementById('letra').style.display = 'none';
			document.getElementById('validar').style.display = 'none';			
		}

		if (intentos == MAXINTENTOS) {
			palabraActual = palabraOculta;
			mostrarInformacion();
			clearInterval(intervalo);
			document.getElementById('mensaje').style.display = 'block';
			document.getElementById('mensaje').style.backgroundColor = 'red';
			document.getElementById('mensaje').style.border = 'thin solid red';
			document.getElementById('mensaje').style.color = 'white';
			document.getElementById('mensaje').innerHTML = 'Ohhhh!!! Perdiste.';
			document.getElementById('letra').style.display = 'none';
			document.getElementById('validar').style.display = 'none';

		}
		
		document.getElementById('letra').focus();
		document.getElementById('letra').value = "";
	}

	function pantalla_reinicio(){

		document.getElementById('cabeza').style.display = 'none';
		document.getElementById('suelo').style.display = 'none';
		document.getElementById('mensaje').style.display = 'none';
		document.getElementById('palo-vertical').style.display = 'none';
		document.getElementById('palo-superior').style.display = 'none';
		document.getElementById('cuerpo').style.display = 'none';
		document.getElementById('brazos').style.display = 'none';
		document.getElementById('piernas').style.display = 'none';
		document.getElementById('cuerda').style.display = 'none';
		document.getElementById('letra').style.display = 'none';
		document.getElementById('numero').style.display = 'none';
	}

	function mostrarInformacion() {

		var palabraVisible = "";
		for ( var i = 0; i < palabraActual.length; i++) {
			if (palabraActual[i] != 0) {
				palabraVisible += "<span> " + palabraOculta.charAt(i)
						+ " </span>";
			} else {
				palabraVisible += "<span> __ </span>";
			}
		}

		var informacionJuego = "<div id='informacionJuego'> ";
		informacionJuego += "<table>";
		informacionJuego += "   <tr>";
		informacionJuego += "      <td> Max. Numero de Intentos </td>";
		informacionJuego += "      <td><strong>" + MAXINTENTOS + "</strong></td>";
		informacionJuego += "   </tr>";
		informacionJuego += "   <tr>";
		informacionJuego += "      <td> Intentos </td>";
		informacionJuego += "      <td><strong>" + intentos + "</strong></td>";
		informacionJuego += "   </tr>";
		informacionJuego += "   <tr>";
		informacionJuego += "      <td> Palabra Visible </td>";
		informacionJuego += "      <td>" + palabraVisible + "</td>";
		informacionJuego += "   </tr>";
		informacionJuego += "   <tr>";
		informacionJuego += "      <td> Letras Utilizadas </td>";
		informacionJuego += "      <td><span>" + letrasUtilizadas
				+ "</span></td>";
		informacionJuego += "   </tr>";
		informacionJuego += "</table>";
		informacionJuego += "</div>";

		document.getElementById('resultado').innerHTML = informacionJuego;
	}

	function recorrerPalabra() {
		for ( var i = 0; i < palabra.length; i++) {
			document.getElementById('resultado').innerHTML = palabra.charAt(i);
		}	   
	}

	function cuentaAtras() {
		var count = 30;
		var intervalo = setInterval(function(){
           count--;
           var numero = document.getElementById('numero');
           numero.value = count
           if(count == 0){
             clearInterval(intervalo);
				document.getElementById('mensaje').style.display = 'none';
				document.getElementById('mensaje').style.display = 'block';
				document.getElementById('mensaje').style.backgroundColor = 'red';
				document.getElementById('mensaje').style.border = 'thin solid red';
				document.getElementById('mensaje').style.color = 'white';
				document.getElementById('mensaje').innerHTML = 'Se te acabó el tiempo.';
           }
        }, 1000);
	}