function relojManecillaHora(pos)
  {
    ctx.lineWidth = 4;
    ctx.strokeStyle = 'rgba(0,0,0, 1)';
    ctx.beginPath();
    radianes=Math.degToRad(180-pos*30); // hora
    x=radiox/1.5*Math.sin(radianes);
    y=radioy/1.5*Math.cos(radianes);
    x=x+xo;
    y=y+yo;
    ctx.moveTo(xo,yo);
    ctx.lineTo(x,y);
    ctx.stroke();
  }

function relojManecillaMinuto(pos)
  {
    ctx.lineWidth = 2;
    ctx.strokeStyle = 'rgba(0,0,0, 1)';
    ctx.beginPath();
    radianes=Math.degToRad(180-pos*6); // minuto
    x=radiox/1.25*Math.sin(radianes);
    y=radioy/1.25*Math.cos(radianes);
    x=x+xo;
    y=y+yo;
    ctx.moveTo(xo,yo);
    ctx.lineTo(x,y);
    ctx.stroke();
  }

function relojManecillaSegundo(pos)
  {
    ctx.lineWidth = 1;
    ctx.strokeStyle = 'rgba(221,64,4, 1)';
    ctx.beginPath();
    radianes=Math.degToRad(180-pos*6); // segundo
    x=radiox*Math.sin(radianes);
    y=radioy*Math.cos(radianes);
    x=x+xo;
    y=y+yo;
    ctx.moveTo(xo,yo);
    ctx.lineTo(x,y);
    ctx.stroke();
  }

  function relojAvanza()
  {
    var f=new Date();
    ctx.clearRect(0,0,xmax,ymax);//borra canvas, hacerla transparente o sin contenido gráfico
    relojCarcasa();
    relojManecillaSegundo(f.getSeconds());
    relojManecillaMinuto(f.getMinutes());
    relojManecillaHora(f.getHours());
  }

  function relojIniciar()
  {
    window.setInterval('relojAvanza()', 1000); 
  }

  function relojCarcasa() // Circulo con puntos 
  {
    ctx.fillStyle = 'rgba(0,0,0, 1)';
    ctx.beginPath();
    for (ang=0; ang<360; ang=ang+30)
      {
        radianes=Math.degToRad(ang);
        x=radiox*Math.sin(radianes);
        y=radioy*Math.cos(radianes);
        x=x+xo;
        y=y+yo;
        ctx.fillRect(x,y,4,4);
      }
    ctx.stroke();
  }