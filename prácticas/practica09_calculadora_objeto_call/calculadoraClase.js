var global = {
	limpiar:false,
	operacion:null,
	resultado:0
};

window.addEventListener("load", (e) =>{
	global.operaciones = document.querySelectorAll(".operacion");
	global.numeros = document.querySelectorAll(".numero");
    global.display = document.querySelector(".display");
	
	for (var i = 0; i < global.operaciones.length; i++) {
		global.operaciones[i].addEventListener("click",(e) =>{
                    if(!global.limpiar || global.operacion == e.target.value){
			switch (global.operacion) {
				case "+":
					global.resultado += parseInt(global.display.value);
					global.display.value = global.resultado;
					break;
                                case "-":
					global.resultado -= parseInt(global.display.value);
					global.display.value = global.resultado;
					break;   
                                case "*":
					global.resultado *= parseInt(global.display.value);
					global.display.value = global.resultado;
					break;  
                                case "/":
					global.resultado /= parseInt(global.display.value);
					global.display.value = global.resultado;
					break;                                      
				default:
					
					global.resultado = parseInt(global.display.value);
			}
                    }
                global.operacion = e.target.value;
                global.limpiar = true;                      
		})
	}
		for (var i = 0; i < global.numeros.length; i++) {
		global.numeros[i].addEventListener("click",(e) =>{
			if(global.limpiar){
				global.display.value ="";
				global.limpiar = false;
			}
        	global.display.value+= e.target.value;
		});
	}

	console.log(global);
});		