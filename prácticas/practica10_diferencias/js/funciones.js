    function disminuir() {
        var barra = document.querySelector("#barra");
        var ancho = parseInt(barra.style.width);
        ancho = ancho - 1;
        if (ancho >= 0) {
            barra.style.width = ancho + "%";
        } else {
            clearInterval(reloj);
            alert('Se te acabó el tiempo, ya tendrás mejor suerte otro día.')
        }
    }

    function timer() {
        var barra = document.querySelector("#barra");
        barra.style.width = "100%";
        reloj = setInterval(disminuir, 300);
    }
    function stop() {
        clearInterval(reloj);
        alert('Reto conseguido');
    }

    function agregar() {
        var barra = document.querySelector("#barra");
        var ancho = parseInt(barra.style.width);
        ancho = ancho + 1;
        if (ancho >= 0) {
            barra.style.width = ancho + "%";
        } else {
            clearInterval(reloj);
            alert('Se te acabó el tiempo, ya tendrás mejor suerte otro día.')
        }
    }
