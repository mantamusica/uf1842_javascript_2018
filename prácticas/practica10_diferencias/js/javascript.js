    window.onload = cargar;

    var reloj;
    var lamina = 0;
    var aciertos = 0;
    var variable = 1;

    var imagenesIzquierda = new Array();
    var imagenesDerecha = new Array();

    var caja1Top = new Array("67px", "85px", "209px");
    var caja1Left = new Array("91px", "120px", "229px");
    var caja1Ancho = new Array("220px", "80px", "49px");
    var caja1Alto = new Array("75px", "43px", "49px");

    var caja2Top = new Array("125px", "5px", "77px");
    var caja2Left = new Array("251px", "171px", "376px");
    var caja2Ancho = new Array("124px", "20px", "49px");
    var caja2Alto = new Array("101px", "-10px", "49px");

    var caja3Top = new Array("160px", "-50px", "106px");
    var caja3Left = new Array("34px", "290px", "290px");
    var caja3Ancho = new Array("115px", "49px", "39px");
    var caja3Alto = new Array("101px", "49px", "39px");

    var caja4Top = new Array("-40px", "30px", "-15px");
    var caja4Left = new Array("152px", "133px", "253px");
    var caja4Ancho = new Array("105px", "49px", "49px");
    var caja4Alto = new Array("68px", "89px", "49px");

    var caja5Top = new Array("-55px", "30px", "-100px");
    var caja5Left = new Array("91px", "62px", "330px");
    var caja5Ancho = new Array("90px", "59px", "39px");
    var caja5Alto = new Array("55px", "75px", "39px");

    //Calcular coordenadas
    /*    function funcion1(event){
            console.log(event.offsetX);
            console.log(event.offsetY);

        }*/



    function acertar(event) {
        event.target.style.backgroundImage = 'url(img/tic.png)';
        agregar();

        aciertos++;

        if (aciertos == 5) {
            cambiar_lamina();
            console.log(aciertos);
        }
        if (aciertos == 10) {
            cambiar_lamina();
            console.log(aciertos);
        }
        if (aciertos == 15) {
            stop();
            console.log(aciertos);
        }
    }

    function cargar() {

        document.querySelector(".derecha").addEventListener("click", disminuir);

        //document.querySelector(".derecha").addEventListener("click",funcion1);
        timer();

        //Precargamos las imágenes
        for (var i = 1; i < 4; i++) {

            imagenesIzquierda[i] = new Image();
            imagenesIzquierda[i].src = 'img/' + i + "bien.png";
        }

        for (var i = 1; i < 4; i++) {

            imagenesDerecha[i] = new Image();
            imagenesDerecha[i].src = 'img/' + i + "mal.png";
        }

        //Visualizar las imágenes

        document.querySelector(".izquierda").style.backgroundImage = 'url(' + imagenesIzquierda[variable].src + ')';
        document.querySelector(".derecha").style.backgroundImage = 'url(' + imagenesDerecha[variable].src + ')';

        document.querySelector(".one").addEventListener("click", acertar);
        document.querySelector(".two").addEventListener("click", acertar);
        document.querySelector(".three").addEventListener("click", acertar);
        document.querySelector(".four").addEventListener("click", acertar);
        document.querySelector(".five").addEventListener("click", acertar);

        document.querySelector(".one").style.border = "none";
        document.querySelector(".two").style.border = "none";
        document.querySelector(".three").style.border = "none";
        document.querySelector(".four").style.border = "none";
        document.querySelector(".five").style.border = "none";
        document.querySelector(".one").style.top = caja1Top[lamina];
        document.querySelector(".one").style.left = caja1Left[lamina];
        document.querySelector(".one").style.width = caja1Ancho[lamina];
        document.querySelector(".one").style.height = caja1Alto[lamina];
        document.querySelector(".two").style.top = caja2Top[lamina];
        document.querySelector(".two").style.left = caja2Left[lamina];
        document.querySelector(".two").style.width = caja2Ancho[lamina];
        document.querySelector(".two").style.height = caja2Alto[lamina];
        document.querySelector(".three").style.top = caja3Top[lamina];
        document.querySelector(".three").style.left = caja3Left[lamina];
        document.querySelector(".three").style.width = caja3Ancho[lamina];
        document.querySelector(".three").style.height = caja3Alto[lamina];
        document.querySelector(".four").style.top = caja4Top[lamina];
        document.querySelector(".four").style.left = caja4Left[lamina];
        document.querySelector(".four").style.width = caja4Ancho[lamina];
        document.querySelector(".four").style.height = caja4Alto[lamina];
        document.querySelector(".five").style.top = caja5Top[lamina];
        document.querySelector(".five").style.left = caja5Left[lamina];
        document.querySelector(".five").style.width = caja5Ancho[lamina];
        document.querySelector(".five").style.height = caja5Alto[lamina];

    }

    function cambiar_lamina() {
        console.log('cambiar_lamina '+aciertos);
        variable++;
        console.log('contador de variable'+variable);
        document.querySelector(".izquierda").style.backgroundImage = 'url(' + imagenesIzquierda[variable].src + ')';
        document.querySelector(".derecha").style.backgroundImage = 'url(' + imagenesDerecha[variable].src + ')';
        document.querySelector(".one").style.backgroundImage = 'none';
        document.querySelector(".two").style.backgroundImage = 'none';
        document.querySelector(".three").style.backgroundImage = 'none';
        document.querySelector(".four").style.backgroundImage = 'none';
        document.querySelector(".five").style.backgroundImage = 'none';
        lamina++;
        document.querySelector(".one").style.top = caja1Top[lamina];
        document.querySelector(".one").style.left = caja1Left[lamina];
        document.querySelector(".one").style.width = caja1Ancho[lamina];
        document.querySelector(".one").style.height = caja1Alto[lamina];
        document.querySelector(".two").style.top = caja2Top[lamina];
        document.querySelector(".two").style.left = caja2Left[lamina];
        document.querySelector(".two").style.width = caja2Ancho[lamina];
        document.querySelector(".two").style.height = caja2Alto[lamina];
        document.querySelector(".three").style.top = caja3Top[lamina];
        document.querySelector(".three").style.left = caja3Left[lamina];
        document.querySelector(".three").style.width = caja3Ancho[lamina];
        document.querySelector(".three").style.height = caja3Alto[lamina];
        document.querySelector(".four").style.top = caja4Top[lamina];
        document.querySelector(".four").style.left = caja4Left[lamina];
        document.querySelector(".four").style.width = caja4Ancho[lamina];
        document.querySelector(".four").style.height = caja4Alto[lamina];
        document.querySelector(".five").style.top = caja5Top[lamina];
        document.querySelector(".five").style.left = caja5Left[lamina];
        document.querySelector(".five").style.width = caja5Ancho[lamina];
        document.querySelector(".five").style.height = caja5Alto[lamina];
        if (variable == 4) {
                alert('conseguido');
        }
    }