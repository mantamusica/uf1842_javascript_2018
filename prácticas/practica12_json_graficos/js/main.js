window.addEventListener("load", function() {
    var datos = new Array(
        {nombre:"Daniel", valor: 40},
        {nombre:"Roberto", valor: 10},
        {nombre:"Ana", valor: 20},
        {nombre:"Jose", valor: 15},
        {nombre:"Silvia", valor: 17},
        {nombre:"Jesús", valor: 25},
        {nombre:"Lola", valor: 14},
        {nombre:"Rosa", valor: 10},
        {nombre:"Carmen", valor: 7},
        {nombre:"Sara", valor: 19}
    );
    // añadir unas funciones al Array de los datos
    // devuelve un array con el atributo nombre de los objetos
    datos.nombres = function(){
        return this.map(function(valor){
            return valor.nombre;
        });
    };
    // devuelve un array con el atributo valor de los objetos
    datos.valores = function(){
        return this.map(function(valor){
            return valor.valor;
        });
    };

    // Objeto raíz Gráfico para manejar canvas.
    function Grafico(canvas) {
        this.elemento = document.querySelector(canvas);
        var ctx = this.elemento.getContext("2d");
        this.ancho = this.elemento.clientWidth;
        this.elemento.width = this.ancho;
        this.alto = this.elemento.clientHeight;
        this.elemento.height = this.alto;
        var coor = {x:0, y:0, xMin:0, yMin:0, xMax:0, yMin:0};
        this.margenTexto = 20;
        this.getCtx = function(){return ctx;};
        this.getCoor = function(){return coor;};
        this.setCoorXMax = function(x){coor.xMax = x;};
        this.setCoorXMin = function(x){coor.xMin = x;};
        this.setCoorX = function(x){coor.x = x;};
        this.setCoorYMax = function(y){coor.yMax = y;};
        this.setCoorYMin = function(y){coor.yMin = y;};
        this.setCoorY = function(y){coor.y = y;};
    };
    Grafico.prototype.borrar = function() {
        this.getCtx().clearRect(0, 0, this.ancho, this.alto);
        return this;
    };
    Grafico.prototype.rect = function(x, y, ancho, alto, color="#000"){
        this.getCtx().fillStyle = color;
        this.getCtx().fillRect(x, y, ancho, alto);
        return this;
    };
    Grafico.prototype.linea = function(x1, y1, x2, y2, width, color="#000"){
        this.getCtx().lineWidth = width;
        this.getCtx().beginPath();
        this.getCtx().moveTo(x1, y1);
        this.getCtx().lineTo(x2, y2);
        this.getCtx().strokeStyle = color;
        this.getCtx().closePath();
        this.getCtx().stroke();
        return this;
    };
    Grafico.prototype.texto = function(x, y, text, align="center", font="14px serif", color="#000"){
        this.getCtx().font = font;
        this.getCtx().textAlign = align;
        this.getCtx().fillStyle = color;
        this.getCtx().fillText(text, x, y);
        return this;
    };
    Grafico.prototype.circulo = function(x, y, radio,color="#000"){
        this.getCtx().beginPath();
        this.getCtx().arc(x,y,radio,0,(Math.PI/180)*360,true);
        this.getCtx().fillStyle=color;
        this.getCtx().closePath();
        this.getCtx().fill();
        return this;
    };
    Grafico.prototype.textWidth = function(text){
        return Math.floor(this.getCtx().measureText(text).width);
    };
    // Los rangos serán iguales tanto para el eje X como para el Y
    // si el máximo valor supera 50 los rangos serán de 10 en 10
    // si es menor serán de 5 en 5
    Grafico.prototype.rangos = function(valores){
        var maximo = Math.max.apply(this, valores);
        if (maximo <= 50){
            var arr = new Array(10).fill(0);
            arr.forEach(function(valor,idx){
                arr[idx] = (idx == 0) ? 0 : arr[idx-1] +5;
            });
        }else{
            var arr = new Array((maximo / 10)+2).fill(0);
            arr.forEach(function(valor,idx){
                arr[idx] = (idx == 0) ? 0 : arr[idx-1] +10;
            });
        }
        return arr;
    };
    // ejeX y ejeY son arrays con los textos para los ejes
    Grafico.prototype.grid = function(ejeX, ejeY){
        var _this = this;
        // medidas de los textos en pixels
        var widthY = ejeY.map(function(valor){
            return _this.textWidth(valor);
        });
        var widthX = ejeX.map(function(valor){
            return _this.textWidth(valor);
        });
        // el máximo valor de los textos en pixeles
        var maxWidthY = Math.max.apply(this, widthY) + this.margenTexto;
        var maxWidthX = Math.max.apply(this, widthX) + this.margenTexto;
        this.incX = this.ancho / ejeX.length;
        this.incY = this.alto / (ejeY.length + 1);
        // TODO: comprobar que el texto cabe, sino colocarlo a dos alturas
        // según sea par o impar
        // dibujamos los textos en el eje X
        this.setCoorX(maxWidthY);
        var acc = (maxWidthY - this.incX) + this.margenTexto;
        ejeX.forEach(function(item,idx,arr){
            _this.texto(acc + _this.incX, _this.alto - _this.margenTexto, item);
            acc += _this.incX;
        });
        // dibujamos los textos en el eje Y
        this.setCoorY(this.alto - this.margenTexto);
        acc = this.getCoor().y;
        ejeY.forEach(function(item,idx,arr){
            _this.texto(maxWidthY, acc - _this.incY, item, "end");
            acc -= _this.incY;
        });
        this.setCoorYMin(((this.getCoor().y - this.incY) -5));
        this.setCoorXMin(this.getCoor().x + this.margenTexto);
        return this;
    };
    // Dibujar las líneas guías en el eje indicado en direccion
    // y en eje? los array de los textos a poner
    Grafico.prototype.guias = function(direccion, ejeX, ejeY){
        var _this = this;
        if (direccion == "horizontal".toLowerCase()){
            var x = _this.getCoor().x + 5;
            var y = this.getCoor().yMin;
            var x2 = this.ancho;
            ejeY.forEach(function(item,idx,arr){
                _this.linea(x, y , x2, y , 1, "#ddd");
                y -= _this.incY;
            });
            this.setCoorYMax(y + this.incY);
        }else{
            var x = this.getCoor().xMin;
            var y = (this.getCoor().y - 15);
            ejeX.forEach(function(item,idx,arr){
                _this.linea(x, y , x, 0, 1, "#ddd");
                x += _this.incX;
            });
            this.setCoorXMax(x - this.incX);
        };
    };

    // Objeto que hereda de Grafico para el canvas de tipo Columnas
    function Columnas(canvas) {
        Grafico.call(this, canvas);
        this.dibujar = function(data){
            var arrayX = data.nombres();
            var arrayY = this.rangos(data.valores());
            this.grid(arrayX,arrayY);
            this.guias("horizontal", arrayX, arrayY);
            this.barras(data.valores());
        };
        this.barras = function(data){
            var yDif = this.getCoor().yMin - this.getCoor().yMax;
            var maximoRango = Math.max.apply(this, this.rangos(data));
            var intervalo = yDif / maximoRango;
            var anchoBarra = 10;
            // x de inicio es la x del primer valor menos la  mitad
            // de la anchura de la barra.
            var x = this.getCoor().xMin - (anchoBarra/2);
            var _this = this;
            data.forEach(function(item,idx,arr){
                var y = _this.getCoor().yMax + (intervalo * (maximoRango - item) );
                _this.rect(x + (_this.incX * idx ), y, anchoBarra, _this.getCoor().yMin - y);
            });
        };
    };
    Columnas.prototype = Object.create(Grafico.prototype);
    Columnas.prototype.constructor = Columnas;

    // Objeto que hereda de Grafico para el canvas de tipo Barras
    function Barras(canvas) {
        Grafico.call(this, canvas);
        this.dibujar = function(data){
            var arrayY = data.nombres();
            var arrayX = this.rangos(data.valores());
            this.grid(arrayX,arrayY);
            this.guias("vertical", arrayX, arrayY);
            this.barras(data.valores());
        };
        this.barras = function(data){
            var xDif = this.getCoor().xMax - this.getCoor().xMin;
            var maximoRango = Math.max.apply(this, this.rangos(data));
            var intervalo = xDif / maximoRango;
            var anchoBarra = 10;
            // x de inicio es la x del primer valor menos la  mitad
            // de la anchura de la barra.
            var y = this.getCoor().yMin - (anchoBarra/2);
            var _this = this;
            data.forEach(function(item,idx,arr){
                _this.rect(_this.getCoor().xMin, y - (_this.incY * idx), intervalo * item, anchoBarra);
            });
        };
    };
    Barras.prototype = Object.create(Grafico.prototype);
    Barras.prototype.constructor = Barras;

    // Objeto que hereda de Grafico para el canvas de tipo Lineas
    function Lineas(canvas) {
        Grafico.call(this, canvas);
        this.dibujar = function(data){
            var arrayX = data.nombres();
            var arrayY = this.rangos(data.valores());
            this.grid(arrayX,arrayY);
            this.guias("horizontal", arrayX, arrayY);
            this.curvas(data.valores());
        };
        this.curvas = function(data){
            var yDif = this.getCoor().yMin - this.getCoor().yMax;
            var maximoRango = Math.max.apply(this, this.rangos(data));
            var intervalo = yDif / maximoRango;
            var anchoCirculo = 10;
            // x de inicio es la x del primer valor menos la  mitad
            // de la anchura del círculo.
            var x = this.getCoor().xMin - (anchoCirculo/2);
            var _this = this;
            // dibujar los círculos
            data.forEach(function(item,idx,arr){
                var y = _this.getCoor().yMax + (intervalo * (maximoRango - item) );
                _this.circulo(x + (_this.incX * idx), y, anchoCirculo);
            });
            // dibujar las líneas que unen los círculos
            data.forEach(function(item,idx,arr){
                var y = _this.getCoor().yMax + (intervalo * (maximoRango - item) );
                var yNext = _this.getCoor().yMax + (intervalo * (maximoRango - arr[idx+1]) );
                _this.linea(x + (_this.incX * idx), y, x + (_this.incX * (idx +1)),  yNext, 2);
            });
        };
    };
    Lineas.prototype = Object.create(Grafico.prototype);
    Lineas.prototype.constructor = Lineas;

    var columnas = new Columnas("#columnas").dibujar(datos);
    var barras = new Barras("#barras").dibujar(datos);
    var lineas = new Lineas("#lineas").dibujar(datos);

});
