window.addEventListener("load", function() {
    var datos = new Array(
        {nombre:"Roberto", valor: 35},
        {nombre:"Ana", valor: 10},
        {nombre:"Jose", valor: 15},
        {nombre:"Silvia", valor: 12},
        {nombre:"Jesús", valor: 13},
        {nombre:"Lola", valor: 15}
    );
    // añadir unas funciones al Array de los datos
    // devuelve un array con el atributo nombre de los objetos
    datos.nombres = function(){
        return this.map(function(valor){
            return valor.nombre;
        });
    };
    // devuelve un array con el atributo valor de los objetos
    datos.valores = function(){
        return this.map(function(valor){
            return valor.valor;
        });
    };

    // Objeto raíz Gráfico para manejar canvas.
    function Grafico(canvas) {
        this.elemento = document.querySelector(canvas);
        var ctx = this.elemento.getContext("2d");
        this.ancho = this.elemento.clientWidth;
        this.elemento.width = this.ancho;
        this.alto = this.elemento.clientHeight;
        this.elemento.height = this.alto;
        var coor = {x:0, y:0, radio:0};
        this.margenTexto = 20;
        this.getCtx = function(){return ctx;};
        this.getCoor = function(){return coor;};
        this.setCoorX = function(x){coor.x = x;};
        this.setCoorY = function(y){coor.y = y;};
        this.setCoorRadio = function(r){coor.radio = r};
    };
    Grafico.prototype.rect = function(x, y, ancho, alto, color="#000"){
        this.getCtx().fillStyle = color;
        this.getCtx().fillRect(x, y, ancho, alto);
        return this;
    };
    Grafico.prototype.linea = function(x1, y1, x2, y2, width, color="#000"){
        this.getCtx().lineWidth = width;
        this.getCtx().beginPath();
        this.getCtx().moveTo(x1, y1);
        this.getCtx().lineTo(x2, y2);
        this.getCtx().strokeStyle = color;
        this.getCtx().closePath();
        this.getCtx().stroke();
        return this;
    };
    Grafico.prototype.texto = function(x, y, text, align="center", font="14px serif", color="#000"){
        this.getCtx().font = font;
        this.getCtx().textAlign = align;
        this.getCtx().fillStyle = color;
        this.getCtx().fillText(text, x, y);
        return this;
    };
    Grafico.prototype.circulo = function(x, y, radio, color="#000"){
        this.getCtx().beginPath();
        this.getCtx().arc(x,y,radio,0,(Math.PI/180)*360,true);
        this.getCtx().fillStyle=color;
        this.getCtx().closePath();
        this.getCtx().fill();
        return this;
    };
    Grafico.prototype.textWidth = function(text){
        return Math.floor(this.getCtx().measureText(text).width);
    };
    Grafico.prototype.sector = function(angInicio, angFinal, color="#000"){

        this.getCtx().beginPath();
        this.getCtx().fillStyle = color;
        this.getCtx().moveTo(this.getCoor().x, this.getCoor().y);
        this.getCtx().arc(this.getCoor().x, this.getCoor().y, this.getCoor().radio, angInicio*Math.PI, angFinal*Math.PI);
        this.getCtx().closePath();
        this.getCtx().fill();
        return this;
    };
    Grafico.prototype.colorAleatorio = function(){
        var random = function(inferior,superior){
            var numPosibilidades = superior - inferior;
            var aleat = Math.random() * numPosibilidades;
            aleat = Math.floor(aleat);
            return parseInt(inferior) + aleat;
            }
        var hexadecimal = new Array("0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F");
        var color_aleatorio = "#";
        for (var i=0;i<6;i++){
            var posarray = random(0,hexadecimal.length);
            color_aleatorio += hexadecimal[posarray];
        }
        return color_aleatorio;
    };
    Grafico.prototype.dibujar = function(data){
        _this = this;
        // el centro del círculo en medio del canvas.
        this.setCoorX(this.ancho / 2);
        this.setCoorY(this.alto / 2);
        this.setCoorRadio(230);
        // este es un círculo de fondo.
        this.circulo(this.getCoor().x, this.getCoor().y, this.getCoor().radio, "#ccc");
        var inicio = 0;
        data.forEach(function(item,idx,arr){
            var final = inicio + (item.valor * 0.02); // 0.02 radianes por 1%
            var color = _this.colorAleatorio();
            _this.sector(inicio, final, color);
            inicio = final;
            _this.rect(0, idx * 30, 15, 15, color);
            _this.texto(20, (idx * 30) + 12, item.nombre + " " + item.valor + "%", "left", );
        });
    };


    var sectores = new Grafico("#grafico").dibujar(datos);

});
