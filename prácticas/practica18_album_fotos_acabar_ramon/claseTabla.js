/*Creamos clase*/
var Tabla = function() {
    this.texto = "Incluimos aquí la foto";
    this.filas = 0;
    this.columnas = 0;
    this.colorBorde = "black";
    this.colorFondo = "white";


    //getter y setter

    this.getTexto = function() {
        return texto;
    }
    this.setTexto = function(valor) {
        texto = valor;
    }
    this.getFilas = function() {
        return filas;
    }
    this.setFilas = function(valor) {
        filas = valor;
    }
    this.getColumnas = function() {
        return columnas;
    }
    this.setColumnas = function(valor) {
        columnas = valor;
    }
    this.getColorBorde = function() {
        return colorBorde;
    }
    this.setColorBorde = function(valor) {
        colorBorde = valor;
    }
    this.getColorFondo = function() {
        return colorFondo;
    }
    this.setColorFondo = function(valor) {
        colorFondo = valor;

        //métodos publicos
        this.dibujar = function() {

        }

        this.colorear = function(borde, fondo) {
            colorBorde = borde;
            colorFondo = fondo;
        }
    }
}