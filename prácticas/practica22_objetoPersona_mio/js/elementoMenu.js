		var ElementoMenu = function(argTexto = "",argEnlace = "#"){
			var texto = "";
			var enlace = "";
			var etiqueta = document.createElement("li");
			var etiquetaEnlace = document.createElement("a");

			this.setTexto = function(arg){
				texto = arg;
				etiquetaEnlace.style.display = 'block';
				etiquetaEnlace.innerHTML = this.getTexto();
			}
			this.setEnlace = function(arg){
				enlace = arg;
				etiquetaEnlace.href = this.getEnlace();
			}
			this.getTexto = function(){
				return texto;
			}	
			this.getEnlace = function(){
				return enlace;
			}
			//this.crearEtiqueta = function(){
				//var etiqueta = document.createElement("li");
				//var etiquetaEnlace = document.createElement("a");
				//etiquetaEnlace.style.display = 'block';
				//etiquetaEnlace.innerHTML = this.getTexto();
			//}
			this.getEtiqueta = function(){
				return etiqueta;
			}

			//constructor
			this.ElementoMenu = function(argTexto, argEnlace){
				this.setTexto(argTexto);
				this.setEnlace(argEnlace);
				etiqueta.appendChild(etiquetaEnlace);
			}

			//llamamos al método constructor
			this.ElementoMenu(argTexto, argEnlace);
		}