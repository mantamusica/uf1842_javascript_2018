		var Menu = function(){
			var elementosMenu = new Array();

			this.setElementosMenu = function(arg){
					elementosMenu.push(arg);
			}
			this.getElementosMenu = function(){
					return elementosMenu;
			}

			this.getEtiqueta = function(){
				var etiquetaUl = document.createElement('ul');

				for (var i = 0; i < elementosMenu.length; i++) {
					etiquetaUl.appendChild(elementosMenu[i].getEtiqueta());
				}
				return etiquetaUl;	
			}
		}