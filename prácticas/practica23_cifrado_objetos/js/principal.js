window.addEventListener("load", function() {

    function Cifrado(numero, texto) {
        var num = numero || 4;
        var frase = texto || "";

        function grupos(str) {
            var tmp = (str.length / num) | 0;
            if ((str.length % num) != 0) tmp += 1;
            return tmp;
        }

        function rotacion(arr) {
            arr.unshift(arr.pop());
            return arr;
        }

        function rotacionArray(arr) {
            return rotacion(arr).join(" ");
        }

        function rotacionString(str) {
            var newArr = new Array(grupos(str)).fill(0);
            var strArr = str.split("");
            return newArr.map(function(item, idx, arr) {
                return rotacion(strArr.splice(0, num)).join("");
            }).join("");
        }
        this.cifrar = function(laFrase) {
            var txt = laFrase || frase;
            var strArray = txt.split(" ");
            var newArr = new Array(grupos(strArray)).fill(0);
            return newArr.map(function(item, idx, arr) {
                var palabras = rotacionArray(strArray.splice(0, num));
                return palabras.split(" ").map(function(it, id, ar) {
                    return rotacionString(it);
                }).join(" ");
            }).join(" ");
        }
    }
    document.entradas.addEventListener("submit", function(evt){
        var frase = document.entradas.frase.value;
        var num = document.entradas.numero.value;
        if (isOK(frase)){
            var cifrar = new Cifrado(num, frase);

        }
    });
});
