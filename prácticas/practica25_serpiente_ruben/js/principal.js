window.addEventListener("load", function() {

/***************************************************************************************
 * Objeto Objeto
 * Param: tamaño del objeto, contexto del canvas, color del Objeto
 * Methods: choca que recibe como parámetro un objeto y devuelve verdadero
 * si hay colisión entre ellos, en caso contrario falso.
 * dibujar que es la rutina básica de dibujo de todos los objetos
 ***************************************************************************************/
    var Objeto = function(tamano, ctx, color){
        this.tamano = tamano;
        this.ctx = ctx;
        this.color = color;
        this.choca =  function(obj){
            var diferenciaX = Math.abs(this.x - obj.x);
            var diferenciaY = Math.abs(this.y - obj.y);
            return (diferenciaX >= 0 && 
                    diferenciaX < this.tamano && 
                    diferenciaY >= 0 && 
                    diferenciaY < this.tamano);
        }
    }
    Objeto.prototype.dibujar = function(){
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect(this.x,this.y,this.tamano,this.tamano);
    }

/****************************************************************************************
 *  Objeto Snake
 *  Param: x coordenada, y coordenada, tamano del objeto, contexto de dibujo y
 *  color del objeto
 *  Methods: setXY para establecer las nuevas coordenadas. nuevaCola para añadir
 *  un objeto nuevo en la cola. getCola para obtener la cola. dibujar que redefine
 *  el método de su padre Objeto
 *  La clave del movimiento de la serpiente está en el la recursividad conseguida
 *  con el puntero cola. Conseguimos una lista enlazada de objetos Snake. El
 *  movimiento se consigue al asignar la posición del anterior al siguiente en la
 *  cola.
 ****************************************************************************************/
    var Snake = function(x,y,tamano,ctx,color){
        Objeto.call(this, tamano, ctx, color);
        this.x = x;
        this.y = y;
        this.cola = null;
        // A la cola le ponemos la coordenada que tiene su antecesor antes de
        // asignar las nuevas coordenadas.
        this.setXY = function(x, y){
            if (this.cola != null) this.cola.setXY(this.x, this.y);
            this.x = x;
            this.y = y;
        }
        // Si es la última cola le añadimos una nueva. Si no llamamos otra vez al
        // método de modo recursivo.
        this.nuevaCola = function(){
            if (this.cola == null) {
                this.cola = new Snake(this.x, this.y, this.tamano, this.ctx, this.color);
            } else {
                this.cola.nuevaCola();
            }
        }
        this.getCola = function(){
            return this.cola;
        }
    }
    Snake.prototype = Object.create(Objeto.prototype);
    Snake.prototype.constructor = Snake;
    // Si hay cola, se llama a esta para que se dibuje
    Snake.prototype.dibujar = function(){
        if (this.cola != null) this.cola.dibujar()
        Objeto.prototype.dibujar.call(this);
    }

/********************************************************************************************
 * Objeto Comida
 * Param: tamano del objeto, contexto para dibujar y color del Objeto
 * Methods: aleatorio para dar coordenadas aleatorias dentro del canvas y
 * nuevoSitio para asignar las coordenadas a las variables.
 */
    var Comida = function(tamano, ctx, color){
        Objeto.call(this, tamano, ctx, color);
        this.x = 0;
        this.y = 0;
        this.aleatorio = function(){
            return {
                x: (Math.random() * (this.ctx.canvas.width / this.tamano) | 0) * this.tamano,
                y: (Math.random() * (this.ctx.canvas.height / this.tamano) | 0) * this.tamano
            }
        }
        this.nuevoSitio = function(){
            var dim = this.aleatorio();
            this.x = dim.x;
            this.y = dim.y;
        }
        this.nuevoSitio();
    }
    Comida.prototype = Object.create(Objeto.prototype);
    Comida.prototype.constructor = Comida;

/**********************************************************************************************
 * Objeto Juego:
 * Es el que controla la dinámica del juego y las relaciones entre la comida y la
 * serpiente y entre la serpiente y los bordes.
 * Se encarga de crear los objetos Comida y Snake.
 * Y de engancharse al DOM para modificar la velocidad y presentar la puntuación.
 **********************************************************************************************/
    var Juego = function(obj) {
        this.canvas = obj.canvas;
        this.ctx = canvas.getContext("2d");
        this.velocidad = obj.velocidad || 10;
        this.tamano = obj.tamano || 10;
        this.sierpe = new Snake(50, 70, this.tamano, this.ctx, "#000000");
        this.comida = new Comida(this.tamano, this.ctx, "#FF0000");
        this.puntos = document.querySelector("#puntuacion");
        this.dirX = true;
        this.dirY = true;
        this.incX = 0;
        this.incY = 0;
        this.animacion = null;
        var _this = this;
        // Obtener la velocidad del boton pulsado. Valor situado en data-vel
        this.setVelocidad = function(evt){
            _this.velocidad = parseInt(evt.target.dataset.vel);
        }
        // Choca la serpiente consigo misma?
        // Para saberlo compruebo que colisionan la cabeza(sierpe) con 
        // alguna de sus colas.
        this.autoChoque = function() {
            var temp = null;
            // Hay que atrapar el error al conseguir la cola por si no hay
            try {
                temp = this.sierpe.getCola().getCola();
            } catch (err) {
                temp = null;
            }
            // mientras haya cola hay que comprobar las colisiones
            while (temp != null) {
                if (this.sierpe.choca(temp)) {
                    this.fin();
                } else {
                    temp = temp.getCola();
                }
            }
        }
        // Choca la serpiente contra la pared?
        this.paredChoque = function() {
            if (this.sierpe.x < 0 ||
                this.sierpe.x > (this.canvas.width - this.sierpe.tamano) ||
                this.sierpe.y < 0 ||
                this.sierpe.y > (this.canvas.height - this.sierpe.tamano)) {
                this.fin();
            }
        }
        this.dibujar = function() {
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.sierpe.dibujar();
            this.comida.dibujar();
        }
        this.mover = function(){
            var nx = this.sierpe.x + this.incX;
            var ny = this.sierpe.y + this.incY;
            this.sierpe.setXY(nx, ny);
        }
        this.addPunto = function(){
            this.puntos.innerHTML = parseInt(this.puntos.innerHTML) +1;
        }
        this.resetPunto = function(){
            this.puntos.innerHTML = 0;
        }
        // La velocidad la mido en Frames por segundo
        // haciendo un bucle vacio para comprobar la diferencia 
        // del tiempo obtenido hasta que llegue al fps deseado (vel)
        this.fps = function() {
            var vel = 1000 / _this.velocidad; // 1000 ms
            var init = new Date().getTime();
            do {} while ((new Date().getTime() - init) < vel);
        }
        // Ciclo principal repetido con requestAnimationFrame
        // y guardamos el handle pàra poder parar 
        this.loop = function() {
            _this.fps();
            _this.autoChoque();
            _this.paredChoque();
            _this.dibujar();
            _this.mover();
            if (_this.sierpe.choca(_this.comida)) {
                _this.comida.nuevoSitio();
                _this.sierpe.nuevaCola();
                _this.addPunto();
            }
            _this.animacion = requestAnimationFrame(_this.loop);
        }
        // Gracias al evento del teclado puedo modificar las variables
        // de dirección dirX y dirY y los incrementos para las coordenadas
        // incX e incY
        // La dirección marcar el eje por el que se puede mover y los incrementos
        // determinan los nuevos valores de las coordenadas que vienen dados por
        // el tamaño, tanto en valores positivos como negativos dependiendo de la
        // dirección
        this.evento = function(evt) {
            var cod = evt.keyCode;
            // si está en el eje X sólo puede moverse en el eje Y
            if (_this.dirX) { 
                if (cod == 38) { // flecha arriba
                    _this.incY = -_this.tamano;
                    _this.incX = 0;
                    _this.dirX = false;
                    _this.dirY = true;
                }
                if (cod == 40) { // flecha abajo
                    _this.incY = _this.tamano;
                    _this.incX = 0;
                    _this.dirX = false;
                    _this.dirY = true;
                }
            }
            // si está en el eje Y sólo puede moverse en el eje X
            if (_this.dirY) {
                if (cod == 37) { // flecha izquierda
                    _this.incY = 0;
                    _this.incX = -_this.tamano;
                    _this.dirY = false;
                    _this.dirX = true;
                }
                if (cod == 39) { // flecha derecha
                    _this.incY = 0;
                    _this.incX = _this.tamano;
                    _this.dirY = false;
                    _this.dirX = true;
                }
            }
        }
        this.fin = function(){
            this.incX = 0;
            this.incY = 0;
            this.dirX = true;
            this.dirY = true;
            this.sierpe = new Snake(50, 70, this.tamano, this.ctx, "#000000");
            this.comida = new Comida(this.tamano, this.ctx, "#FF0000");
            this.resetPunto();
            alert("¡¡ Perdiste !!");
        }
        document.body.addEventListener("keydown", this.evento);
        document.querySelectorAll(".boton").forEach(function(obj){
            obj.addEventListener("click", _this.setVelocidad);
        })
        this.loop();
    }

/********************************************************************************************
 * Inicio del programa. 
 * Inicio el elemento canvas con las dimensiones de la capa contenedora
 ********************************************************************************************/

    var div = document.querySelector(".canvas");
    var canvas = document.createElement("canvas");
    canvas.width = div.clientWidth;
    canvas.height = div.clientHeight;
    div.appendChild(canvas);

    var juego = new Juego({
        canvas: canvas,
        velocidad: 20, // valores más pequeños, menos velocidad, en fps
        tamano: 10
    });
});
