var global = {
    foto: "img/0.jpg",
    texto: "Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio\n\
            mientras que mira su diseño.",
    titulo: "Deportes"
};
var contador = 9;
var divColumna = document.createElement("DIV");
var enlace = document.createElement("a");
//var divTituloCaption = documentElement("DIV");
//var divCaption = documentElement("P");
var divImagen = document.createElement("IMG");
var divTitulo = document.createElement("H2");
var divTexto = document.createElement("P");

function crearColumna(divColumna, divImagen, divTitulo, divTexto) {

    divColumna.className = "col-lg-4";

    divImagen.className = "img-thumbnail";
    divImagen.src = global.foto;
    enlace.setAttribute("data-toggle", "lightBox");
    enlace.setAttribute("data-target", "#lightbox");
    divImagen.appendChild(enlace);

    divTitulo.className = "bg-primary";
    divTitulo.innerHTML = global.titulo;

    divTexto.innerHTML = global.texto;

    divColumna.appendChild(divImagen);
    divColumna.appendChild(divTitulo);
    divColumna.appendChild(divTexto);

    return divColumna;

}
;
function crearRow(divColumna, contador, cajaSalida) {

    var salida = document.querySelector(cajaSalida);
    for (var i = 0; i < contador; i++) {
        salida.appendChild(crearColumna(divColumna, divImagen, divTitulo, divTexto).cloneNode(true));
    }

}
;


