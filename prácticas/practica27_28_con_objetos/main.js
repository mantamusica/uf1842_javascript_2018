window.addEventListener("load", function(){
	 var menuPpal=document.querySelectorAll("#menu a");
	 for(var c=0; c<menuPpal.length; c++){
	 	menuPpal[c].addEventListener("click", cambiar);
	 }

    menuPpal[0].setAttribute("class", "current");

    crearMetodos();

	ponerEncabezado("inicio");
	ponerNoticias("inicio");

});

function cambiar(){
	/* recoger texto del enlace pulsado y ponerle guión bajo si es mayor de 1 palabra */
	var txt=event.target.innerHTML.toLowerCase();
	txt=txt.split(" ");
	if(txt.length>1){
		txt[0]=txt[0] + "_";
	}
	txt=txt.join("");

	ponerEncabezado(txt);
	ponerNoticias(txt);
	datos.getMenus();

	//Poner class current a pestaña del menú activa
	document.querySelectorAll("#menu a").forEach(function(item){
		item.removeAttribute("class", "current");
		});
	event.target.parentNode.setAttribute("class", "current")
}

function crearMetodos(){

    datos.encabezado = function(pagina){
        return this.getPagina(pagina).filter(function(item){
            return (item.tipos == "Encabezado");
        })[0];
    }
    datos.getPagina = function(pagina){

        return datos.filter(function(item){
            return (item.pagina == pagina);
        })
    }

    datos.getMenus = function(){
        var menus = datos.map(function(item){
           return item.pagina;
        });
        var unico = menus.filter(function(item, pos, arr) {
            return (arr.indexOf(item) == pos);
            });
        return unico.map(function(item){
            var tmp = item.split('_');
            return (tmp.length > 1) ? tmp[1] : tmp[0];
        });

    }

      datos.noticias = function(pagina) {
        var noticias = this.getPagina(pagina).filter(function(item) {
            return (item.tipos == "Noticias");
        });

        noticias.forEach(function(item, idx, arr) {
            item.grande = function() {
                return (item.texto.length > 40);
            }
            item.miniTexto = function() {
                return item.texto.slice(0,39) + " ...";
            }
        });
        return noticias;
    }
}

function lightBox(obj){
    var img = document.createElement("img");
    img.src = obj.foto_grande;
    var txt = document.createElement("p");
    txt.innerHTML = obj.texto;
    var titulo = document.createElement("h3");
    titulo.innerHTML = obj.titulo;
    var cerrar = document.createElement("button");
    cerrar.innerHTML = "Cerrar";
    var div = document.createElement("div");
    div.appendChild(titulo);
    div.appendChild(img);
    div.appendChild(txt);
    div.appendChild(cerrar);
    var contenedor = document.createElement("div");
    contenedor.appendChild(div);
    cerrar.addEventListener("click", function(event){
        event.preventDefault();
        document.body.removeChild(contenedor);
    })
    contenedor.style.background = "rgba(0,0,0,0.8)";
    contenedor.style.position = "fixed";
    contenedor.style.top = "0";
    contenedor.style.left = "0";
    contenedor.style.width = "100%";
    contenedor.style.height = "100%";
    contenedor.style.zIndex = "1000";
    contenedor.style.display = "flex";
    contenedor.style.justifyContent = "center";
    contenedor.style.alignItems = "center";
    div.style.margin = "0 auto";
    div.style.width = "30%";
    img.style.width = "100%";
    txt.style.textAlign = "justify";
    txt.style.fontSize = "20px";
    cerrar.style.fontSize = "15px"
    return contenedor;
}



function ponerEncabezado(arg){
	var encabezado=datos.encabezado(arg);

	document.querySelector("#pitch h1").innerHTML=encabezado.titulo;
	document.querySelector("#pitch p").innerHTML=encabezado.texto;

}

function ponerNoticias(arg){
	var noticias=datos.noticias(arg);
	var enlace=document.querySelectorAll(".more a");

	for(var c=0; c<noticias.length; c++){
		enlace[c].style.display="none";
		document.querySelectorAll(".column h3")[c].innerHTML=noticias[c].titulo;
		document.querySelectorAll(".column img")[c].src=noticias[c].foto;

		 if (noticias[c].grande()) {
        	document.querySelectorAll(".column p:first-of-type")[c].innerHTML=noticias[c].miniTexto();
        	enlace[c].style.display="inline";
        	/* variable temporal para que funcione en escuchador !!! */
        	var tmp=noticias[c];
        	enlace[c].addEventListener("click",function(){
        		//console.log(tmp);
        		document.querySelector("body").appendChild(lightBox(tmp));
        	})

   		 }else{
   		 	document.querySelectorAll(".column p:first-of-type")[c].innerHTML=noticias[c].texto;
   		 }

	}

}

